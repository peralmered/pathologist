# Pathologist (Python 2 version)

(for Python 3 version, see https://bitbucket.org/peralmered/py3_pathologist/src/master/ )

### NOTE: This project will be migrated to Python 3, and the Python 3 version above will be deleted.

is an editor for motion patterns, using lines and bezier curves to build a complex path.

Exports to dc.w statements for use in 68000 assembly projects on Atari 16/32-bit machines.


## Requirements

* Python v2.7.x

* PyGame v1.9.4 or newer

        pip install pygame

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


## Usage

Pathologist creates *paths*. A path consists of *elements*. There are two types of elements: *lines* and *curves*.

The difference between a line and a curve is that a... curve isn't... necessarily straight. To control the curve, it has
two circular handles. A line doesn't have curve handles.

By CTRL-clicking, you place *vertices* (points). A line or curve runs from one vertex to another. Each vertex has a
speed value, and the movement speed changes smoothly from one vertex to the next. Vertices have square handles.


### Editing

**CTRL+LMB** (left mouse button) to place vertices (if none, you place the first
vertex, if there are existing vertices, you'll add to the end of the path)

**Mousewheel** to zoom in/out

**Right/Left** to go to next/previous element

**L/C** to change current element to a line (**L**) or curve (**C**)

**LMB** to grab vertex or curve handle under mouse pointer, LMB again to place

**Del** (or **X**) to delete current element

**I** to insert element (subdivides current element)

**PageUp/PageDown** to change the number of steps in a curve (**SHIFT** to make smaller increments/decrements)


#### Changing speed

**Up/Down** to change current vertex speed (speed is expressed in pixels/vbl) (**SHIFT** to make smaller increments/decrements)

**0**-**9** to quick-set current vertex speed

**R** to round off current speed value

**V** to change speed for *all* vertices at once, **Enter** to apply when done.


### Previewing

**P** to see path as plotted pixels

**A** to see path animated. During animated preview, use Up/Down to change framerate


### Disk

**S** to save path to disk

**O** to open path from disk


### CLI mode

To convert a .path to .s, run Pathologist.py from the command line::

    python Pathologist.py -i myfile.path [-o myoutputfile.s] [actions]

(if you omit the -o parameter, Pathologist will just export to *myfile.s*)

This will result in a standard 68000 source file with dc.w statements.


#### CLI mode switches


##### -nx (or --no-x)

Will skip all X values in output


##### -ny (or --no-y)

Will skip all Y values in output


## Tips and tricks

You may want to get a specific number of output values, rather than a specific timing. The P key lets you preview your path as dots, and will report the number of points generated.


## Troubleshooting

It is possible to place vertices and handles on top of other vertices/handles.
Press **T** to cycle between all the current elements' vertices/handles, and
press **G** to grab it with the mouse pointer. Finally **LMB** to place.


## Known issues

* When entering speed values from keyboard, numpad entries don't work. I blame PyGame.

* Pressing Space to exit "wait-for-click" screen doesn't work. I blame PyGame.

* Sometimes a curve will look jagged, as it's made out of line elements and not a smooth curve. Technically, curves in Pathologist *are* a number of line elements, and you can use PageUp/PageDown to modify the number of steps a curve will use. PageUp/PageDown will change the number of steps in 10's, holding shift will change it in 1's.


---


# path_mod.py

is a CLI-only tool for modifying .path files.


## Requirements

* Python v2.7.x

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


## Usage

    python path_mod.py -i myfile.path -o newfile.path [actions]


### All switches


#### -i (or --infile) *input_file* **required**

Input .path file


#### -o (or --outfile) *output_file* **required**

Output .path file


#### -soa (or --speed-offset-add) *value*

Adds *value* to all speed values


#### -som (or --speed-offset-mul) *value*

Multiplies all speed values by *value*


#### -xo (or --x-offset) *value*

Adds *value* to all X positions


#### -yo (or --y-offset) *value*

Adds *value* to all Y positions


#### -rx (or --reverse-x)

Flips all X values


#### -ry (or --reverse-y)

Flips all Y values


#### -d (or --dryrun)

Dry run (no writing to disk)


#### -v (or --verbose)

Shows additional information during run

