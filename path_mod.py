#!/usr/bin/env python
# -*- coding: utf-8 -*-

strVersion="0.2"
strProgramHeaderShort="path_mod.py v"+strVersion
strProgramHeader="---[ "+strProgramHeaderShort+" ]------------------------------------------------"
strProgramDescription="[ tool to modify values in .path file ]"

"""

===============================================================================================
===============================================================================================
== ToDo


== ToDo
===============================================================================================
===============================================================================================
== Done


== Done
===============================================================================================
===============================================================================================
== Thoughts


== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True
if boolAllowScreenOutput: print strProgramHeader

boolDebug=True
boolDebugPrinting=False

################################################################################################
################################################################################################
## Defines


## Defines
################################################################################################
################################################################################################
## Constants


## Constants
################################################################################################
################################################################################################
## Globals


## Globals
################################################################################################
################################################################################################
## Functions


## Functions
################################################################################################
################################################################################################
## Libraries


if boolDebug:
  print "Importing libraries...",
import sys
import os
import subprocess
#from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy
import random
import math

sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out
#
#import pdb

import json

if boolDebug:
  print "done\n"


## Libraries
################################################################################################
################################################################################################
## Test code


if 1==2:
  exit(0)


## Test code
################################################################################################
################################################################################################
## Handle input parameters


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="file to load",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="file to save",
                       metavar="FILE",
                       required=True)

objParser.add_argument("-soa", "--speed-offset-add",
                       dest="speedoffsadd",
                       help="speed offset to add to existing values",
                       metavar="STRING",
                       required=False)
objParser.add_argument("-som", "--speed-offset-mul",
                       dest="speedoffsmul",
                       help="speed offset to multiply existing values with",
                       metavar="STRING",
                       required=False)
objParser.add_argument("-xo", "--x-offset",
                       dest="xoffset",
                       help="offset to add to x values",
                       metavar="STRING",
                       required=False)
objParser.add_argument("-yo", "--y-offset",
                       dest="yoffset",
                       help="offset to add to y values",
                       metavar="STRING",
                       required=False)
objParser.add_argument("-rx", "--reverse-x",
                       dest="reversex",
                       help="flip x values",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-ry", "--reverse-y",
                       dest="reversey",
                       help="flip y values",
                       action="store_const",
                       const=True,
                       required=False)


objParser.add_argument("-d", "--dryrun",
                       dest="dryrun",
                       help="dry run (no writing to disk)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)


#objParser.add_argument("-t", "--test",
#                       dest="testdecnum",
#                       help="test of decimal numbers",
#                       metavar="STRING",
#                       required=False)

#objParser.add_argument("-c", "--color-format",
#                       dest="colorformat",
#                       help="palette color format, 'ste', 'tt' or 'falcon' (default is 'ste')",
#                       metavar="STRING",
#                       choices=(COLOR_FORMAT_STE, COLOR_FORMAT_TT, COLOR_FORMAT_FALCON),
#                       required=False)
#objParser.add_argument("-b", "--bitplanes",
#                       dest="bitplanes",
#                       help="number of bitplanes of destination screen (1, 2, 4 or 8, default is 4)",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-s", "--source-bitplanes",
#                       dest="source_bitplanes",
#                       help="number of bitplanes of source, 1-8, default is based on gif's palette size",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-p", "--padding",
#                       dest="padding",
#                       help="number of pixels to pad width to (will auto-adjust to total width of even 16ths by padding zeroes) (default is 0)",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-l", "--loop",
#                       dest="loop",
#                       help="enable looping (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-d", "--doublebuffer",
#                       dest="doublebuffer",
#                       help="double-buffer mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-bk", "--background",
#                       dest="background",
#                       help="background mode enabled (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-n", "--info",
#                       dest="info",
#                       help="info mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-nx", "--info-extra",
#                       dest="infoextra",
#                       help="info-extra mode (warning, can output dozens of MB of text) (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-db", "--debugmode",
#                       dest="debugmode",
#                       help="debug mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
#objParser.add_argument("-q", "--quiet",
#                       dest="quiet",
#                       help="no output during processing, except errors (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""

boolSpeedOffsetAddGiven=False
strSpeedOffsetAdd=""
numSpeedOffsetAdd=0
boolSpeedOffsetMulGiven=False
strSpeedOffsetMul=""
numSpeedOffsetMul=0
boolXOffsetGiven=False
strXOffset=""
numXOffset=0
boolYOffsetGiven=False
strYOffset=""
numYOffset=0
boolReverseX=False
boolReverseY=False

#boolTestDecNumGiven=False
#strDecNum=""

boolDryRun=False

#boolDontUnpackGif=False
#boolUnpackGif=True
#boolDoubleBuffer=False
#boolLoopMode=False
#boolInfoMode=False
#boolInfoExtraMode=False
boolVerboseMode=False
#boolQuietMode=False
#strColorFormat=COLOR_FORMAT_STE
#numScreenBitplanes=4
#boolSourceBitplanes=False
#numSourceBitplanes=0
#boolPaddingGiven=False
#boolForcedPadding=False
#numPadding=0
#boolEnableBackgroundMode=False
#boolDebugMode=False

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue

  elif key=="speedoffsadd" and strValue!="None":
    boolSpeedOffsetAddGiven=True
    strSpeedOffsetAdd=strValue
  elif key=="speedoffsmul" and strValue!="None":
    boolSpeedOffsetMulGiven=True
    strSpeedOffsetMul=strValue
  elif key=="xoffset" and strValue!="None":
    boolXOffsetGiven=True
    strXOffset=strValue
  elif key=="yoffset" and strValue!="None":
    boolYOffsetGiven=True
    strYOffset=strValue
  elif key=="reversex" and strValue!="None":
    boolReverseX=xia.StrToBool(strValue)
  elif key=="reversey" and strValue!="None":
    boolReverseY=xia.StrToBool(strValue)

  elif key=="dryrun" and strValue!="None":
    boolDryRun=xia.StrToBool(strValue)
#  elif key=="testdecnum" and strValue!="None":
#    boolTestDecNumGiven=True
#    strDecNum=strValue

#  elif key=="colorformat" and strValue!="None":
#    strColorFormat=strValue
#  elif key=="bitplanes" and strValue!="None":
#    numScreenBitplanes=int(strValue)
#  elif key=="source_bitplanes" and strValue!="None":
#    boolSourceBitplanes=True
#    numSourceBitplanes=int(strValue)
#  elif key=="padding" and strValue!="None":
#    boolPaddingGiven=True
#    numPadding=int(strValue)
#  elif key=="loop" and strValue!="None":
#    boolLoopMode=xia.StrToBool(strValue)
#  elif key=="doublebuffer" and strValue!="None":
#    boolDoubleBuffer=xia.StrToBool(strValue)
#  elif key=="background" and strValue!="None":
#    boolEnableBackgroundMode=xia.StrToBool(strValue)
#  elif key=="info" and strValue!="None":
#    boolInfoMode=xia.StrToBool(strValue)
#  elif key=="infoextra" and strValue!="None":
#    boolInfoMode=xia.StrToBool(strValue)
#    boolInfoExtraMode=xia.StrToBool(strValue)
#  elif key=="debugmode" and strValue!="None":
#    boolDebugMode=xia.StrToBool(strValue)
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=xia.StrToBool(strValue)
#  elif key=="quiet" and strValue!="None":
#    boolQuietMode=xia.StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)


# Illegal combination of --speed-offset-add and --speed-offset-mul
if boolSpeedOffsetAddGiven and boolSpeedOffsetMulGiven:
  Fatal("You can't give both a --speed-offset-add and --speed-offset-mul!")

# Bad --speed-offset-add value
if boolSpeedOffsetAddGiven:
  try:
    numSpeedOffsetAdd=float(strSpeedOffsetAdd)
  except:
    Fatal("Can't make numerical sense of \""+str(strSpeedOffsetAdd)+"\"!\n(note that decimal numbers must be given as \"3.14\", not \"3,14\")")
  #print "numSpeedOffsetAdd: "+str(numSpeedOffsetAdd)

# Bad --speed-offset-mul value
if boolSpeedOffsetMulGiven:
  try:
    numSpeedOffsetMul=float(strSpeedOffsetMul)
  except:
    Fatal("Can't make numerical sense of \""+str(strSpeedOffsetMul)+"\"!\n(note that decimal numbers must be given as \"3.14\", not \"3,14\")")
  #print "numSpeedOffsetMul: "+str(numSpeedOffsetMul)

# Bad --x-offset value
if boolXOffsetGiven:
  try:
    numXOffset=int(strXOffset)
  except:
    Fatal("Can't make numerical sense of \""+str(strXOffset)+"\"!\n(note that xoffset must be an integer)")
  #print "numXOffset: "+str(numXOffset)

# Bad --y-offset value
if boolYOffsetGiven:
  try:
    numYOffset=int(strYOffset)
  except:
    Fatal("Can't make numerical sense of \""+str(strYOffset)+"\"!\n(note that yoffset must be an integer)")
  #print "numYOffset: "+str(numYOffset)

# Both --x-offset and --reverse-x supplied
if boolXOffsetGiven and boolReverseX:
  Notice("Both --x-offset and --reverse-x given. Please note that --reverse-x is applied first.")

# Both --y-offset and --reverse-y supplied
if boolYOffsetGiven and boolReverseY:
  Notice("Both --y-offset and --reverse-y given. Please note that --reverse-y is applied first.")


## Handle input parameters
################################################################################################
################################################################################################
## Reading input data


# Test infile
numResult=xia.CheckFileExistsAndReadable(strInFile)
if numResult!=0:
  if numResult&xia.FILE_EXISTS_FAIL:
    Fatal("Can't find file \""+strInFile+"\"!")
  if numResult&xia.FILE_READABLE_FAIL:
    Fatal("Can't read file \""+strInFile+"\"!")


Out("Reading input file \""+strInFile+"\"... ")
arrJSON=xia.GetTextFile(strInFile)
strJSON="".join(arrJSON)
arrJSON=json.loads(strJSON)
Out("done.")
Out("\n")


numTargetScreenHeight=arrJSON["target_screen_height"]
numTargetScreenWidth=arrJSON["target_screen_width"]


## Reading input data
################################################################################################
################################################################################################
## Parsing test


if 1==2:
  for key, value in arrJSON.items():
    if isinstance(value, list):
      print str(key)+":"
      for l2 in value:
        for k2, v2 in l2.items():
          if isinstance(v2, list):
            print " "*4+str(k2)+":"
            for n in v2:
              print " "*8+str(n)
          else:
            print " "*4+str(k2)+" == "+str(v2)
    else:
      print str(key)+" == "+str(value)
  print "-"*78


## Parsing test
################################################################################################
################################################################################################
## Reverse X


if boolReverseX:
  for j,e in enumerate(arrJSON["elements"]):
    #print "-"*32
    arrCoords=e["coords"]
    #print str(arrCoords)
    numX0=arrCoords[0]
    numY0=arrCoords[1]
    numX0=numTargetScreenWidth-numX0
    numY0=numTargetScreenHeight-numY0
    arrJSON["elements"][j]["coords"][0]=numX0
    #arrJSON["elements"][j]["coords"][1]=numY0
    if len(arrCoords)>2:
      numX1=arrCoords[2]
      numY1=arrCoords[3]
      numX2=arrCoords[4]
      numY2=arrCoords[5]
      numX1=-numX1
      numY1=-numY1
      numX2=-numX2
      numY2=-numY2
      arrJSON["elements"][j]["coords"][2]=numX1
      #arrJSON["elements"][j]["coords"][3]=numY1
      arrJSON["elements"][j]["coords"][4]=numX2
      #arrJSON["elements"][j]["coords"][5]=numY2
    #print arrJSON["elements"][j]["coords"]


## Reverse X
################################################################################################
################################################################################################
## Reverse Y


if boolReverseY:
  for j,e in enumerate(arrJSON["elements"]):
    #print "-"*32
    arrCoords=e["coords"]
    #print str(arrCoords)
    numX0=arrCoords[0]
    numY0=arrCoords[1]
    numX0=numTargetScreenWidth-numX0
    numY0=numTargetScreenHeight-numY0
    #arrJSON["elements"][j]["coords"][0]=numX0
    arrJSON["elements"][j]["coords"][1]=numY0
    if len(arrCoords)>2:
      numX1=arrCoords[2]
      numY1=arrCoords[3]
      numX2=arrCoords[4]
      numY2=arrCoords[5]
      numX1=-numX1
      numY1=-numY1
      numX2=-numX2
      numY2=-numY2
      #arrJSON["elements"][j]["coords"][2]=numX1
      arrJSON["elements"][j]["coords"][3]=numY1
      #arrJSON["elements"][j]["coords"][4]=numX2
      arrJSON["elements"][j]["coords"][5]=numY2
    #print arrJSON["elements"][j]["coords"]


## Reverse Y
################################################################################################
################################################################################################
## X offset


# X offset
if boolXOffsetGiven:
  for j,e in enumerate(arrJSON["elements"]):
    #print "-"*32
    arrCoords=e["coords"]
    #print str(arrCoords)
    numX0=arrCoords[0]
    numY0=arrCoords[1]
    numX0=numX0+numXOffset
    numY0=numY0+numYOffset
    arrJSON["elements"][j]["coords"][0]=numX0
    #arrJSON["elements"][j]["coords"][1]=numY0


## X offset
################################################################################################
################################################################################################
## Y offset


# Y offset
if boolYOffsetGiven:
  for j,e in enumerate(arrJSON["elements"]):
    #print "-"*32
    arrCoords=e["coords"]
    #print str(arrCoords)
    numX0=arrCoords[0]
    numY0=arrCoords[1]
    numX0=numX0+numXOffset
    numY0=numY0+numYOffset
    #arrJSON["elements"][j]["coords"][0]=numX0
    arrJSON["elements"][j]["coords"][1]=numY0


## Y offset
################################################################################################
################################################################################################
## Speed offset - add


if boolSpeedOffsetAddGiven:
  for j,e in enumerate(arrJSON["elements"]):
    numCurrentSpeed=e["speed"]
    numNewSpeed=numCurrentSpeed+numSpeedOffsetAdd
    #print "speed: "+str(numCurrentSpeed)+"  ==> "+str(numNewSpeed)
    arrJSON["elements"][j]["speed"]=numNewSpeed


## Speed offset - add
################################################################################################
################################################################################################
## Speed offset - multiply


if boolSpeedOffsetMulGiven:
  for j,e in enumerate(arrJSON["elements"]):
    numCurrentSpeed=e["speed"]
    numNewSpeed=numCurrentSpeed*numSpeedOffsetMul
    #print "speed: "+str(numCurrentSpeed)+"  ==> "+str(numNewSpeed)
    arrJSON["elements"][j]["speed"]=numNewSpeed


## Speed offset - multiply
################################################################################################
################################################################################################
## Write to disk


if boolDryRun:
  Notice("Dry run, so no writing to disk")
else:
  Out("Saving file \""+strOutFile+"\"... ")
  strJSON=json.dumps(arrJSON, indent=2, separators=(",", ": "))
  arrJSON=strJSON.split("\n")
  xia.WriteTextFile(arrJSON, strOutFile)
  Out("done.")
  Out("\n\n")


## Write to disk
################################################################################################
################################################################################################



