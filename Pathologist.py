#!/usr/bin/env python
# -*- coding: utf-8 -*-

strVersion="0.993"
strProgramHeaderShort="Pathologist.py v"+strVersion
strProgramHeader="---[ "+strProgramHeaderShort+" ]------------------------------------------------"
strProgramDescription="[ Path editor for movement ]"

"""

===============================================================================================
===============================================================================================
== ToDo

https://bitbucket.org/peralmered/pathologist/issues?status=new&status=open

== ToDo
===============================================================================================
===============================================================================================
== Thoughts


== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True

if boolAllowScreenOutput: print strProgramHeader

boolDebug=True

boolDebugPrinting=False

################################################################################################
################################################################################################
## Globals


boolInterpolateSpeedAcrossElements=True
boolSpaceToDebugStuff=False


###############################################################################
## Sizes of stuffs and ...things


numScreenWidth=0
numScreenHeight=0

numWindowWidth=1800
numWindowHeight=960

numTargetScreenWidth=320
numTargetScreenHeight=200

numTargetScaleIndex=2
arrTargetScales=[]
arrTargetScales.append({"str": "16:1", "val": 16.0})
arrTargetScales.append({"str": "8:1", "val": 8.0})
arrTargetScales.append({"str": "4:1", "val": 4.0})
arrTargetScales.append({"str": "2:1", "val": 2.0})
arrTargetScales.append({"str": "1:1", "val": 1.0})
arrTargetScales.append({"str": "1:2", "val": 0.5})
arrTargetScales.append({"str": "1:4", "val": 0.25})
arrTargetScales.append({"str": "1:8", "val": 0.125})
numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]

numTargetBorderSize=2

numInfoFontSize=20
numInfoLineHeight=24
numInfoPositionX=20
numInfoPositionY=20

numSquareHandleSize=16
numCircleHandleRadius=20/2
numHandleLineWidth=4

numDefaultSpeed=2.0

numDefaultBezierSteps=20
numBezierStepsMinimum=3

numMinimumSpeed=1.0/50.0


## Sizes of stuffs and ...things
###############################################################################
## Colors


colBackground=64,64,64
colTargetWindow=0,0,0
colTargetFrame=191,191,191
colText=255,255,255
colLineDuringCreation=255,255,0
colLineRegular=127,127,127
colLineSelected=255,255,255
colHandleLineEnd=127-32,191-32,127-32
colHandleLineEndSelected=191,255,191
colHandleCurve=127-32,127-32,191-32
colHandleCurveSelected=191,191,255
colHandleShadow=0,0,0
colBezierHelpline=127,127,191

colPreviewPlot=255,127,127


## Colors
###############################################################################
## Other ...entities


numEditorFrameRate=120.0
numPlaybackFrameRate=50.0
numFrameRate=numEditorFrameRate
screen=None
myClock=None
fontInfo=None

twtopx=None
twtopy=None

arrElements=[]

numMode=0  # Hardcoded

numStartX=None
numStartY=None

numCurrentElement=None
numCurrentHandle=2

strWaitingForClickMessage=""

numEditedSpeed=0

arrPlots=[]
arrOutPlots=[]

arrNumKeysValues=[]
arrNumKeys=[]

numAnimFrame=0

strLatestPathFilename=""
strLatestExportFilename=""


## Other ...entities
###############################################################################


## Globals
################################################################################################
################################################################################################
## Defines


n=0
ELEMENT_TYPE_BEZIER=n
n+=1
ELEMENT_TYPE_LINE=n
n+=1
ELEMENT_TYPE_START=n


#----------------------------------------------------------------------------------------------


n=0
EDITOR_MODE_NORMAL=n  # Hardcoded, change the other ones around willynilly...ly if you must, but let this be 0
n+=1
EDITOR_MODE_PLACE_LINE_START=n
n+=1
EDITOR_MODE_PLACE_LINE_END=n
n+=1
EDITOR_MODE_VERTEX_GRAB=n
n+=1
EDITOR_MODE_WAIT_FOR_CLICK=n
n+=1
EDITOR_MODE_SET_SPEED=n
n+=1
EDITOR_MODE_PLOT_PREVIEW=n
n+=1
EDITOR_MODE_ANIMATED_PREVIEW=n
n+=1
EDITOR_MODE_QUIT_ARE_YOU_SURE=n


#----------------------------------------------------------------------------------------------


n=0
HANDLE_TYPE_VERTEX=n
n+=1
HANDLE_TYPE_CURVE=n


#----------------------------------------------------------------------------------------------


HANDLE_ID_OTHER_END_CURVE=0
HANDLE_ID_THIS_END_CURVE=1
HANDLE_ID_VERTEX=2


## Defines
################################################################################################
################################################################################################
## Constants


## Constants
################################################################################################
################################################################################################
## Functions


def HandleTypeToString(this):
  numType=this
  if numType==HANDLE_TYPE_VERTEX:
    return "Vertex"
  elif numType==HANDLE_TYPE_CURVE:
    return "Curve"
  else:
    return "Unknown!"


#----------------------------------------------------------------------------------------------


def TypeToString(this):
  numType=this
  if numType==ELEMENT_TYPE_BEZIER:
    return "Bezier"
  elif numType==ELEMENT_TYPE_LINE:
    return "Line"
  elif numType==ELEMENT_TYPE_START:
    return "Start"
  else:
    return "Unknown!"


#----------------------------------------------------------------------------------------------


def GetLineBrokenString(strIn, numMaxWidth):
  arrOut=[]
  numWidth, numHeight=fontInfo.size(strIn)
  if numWidth>numMaxWidth:
    # Find a space position that leaves us with a short enough string
    arrSpacePositions=[]
    for i, c in enumerate(strIn):
      if c==" ":
        arrSpacePositions.append(i)
    if arrSpacePositions:
      arrSpacePositions=list(reversed(arrSpacePositions))
      boolHit=False
      for ii, p in enumerate(arrSpacePositions):
        strLeftText=strIn[:p]
        #strRightText=strText[p+1:]
        numW, numH=fontInfo.size(strLeftText)
        if numW<numMaxWidth:
          boolHit=True
          numSpacePos=p
          break
      if boolHit:
        strLeftText=strIn[:numSpacePos]
        strRightText=strIn[numSpacePos+1:]
        arrOut.append(strLeftText)
        arrOut.append(strRightText)
        return arrOut
    else:
      # No spaces found, just accept the string as-is
      arrOut.append(strIn)
      return arrOut
  arrOut.append(strIn)
  return arrOut


#----------------------------------------------------------------------------------------------


def DrawText(strText, x, y, colText):
  numMaxTextWidth=numWindowWidth-(numInfoPositionX*2)

  #############################################################
  ## Insert line breaks if necessary  

  arrLineBrokenText=[]
  arrText=strText.split("\n")
  for t in arrText:
    boolLoop=True
    while boolLoop:
      arrResult=GetLineBrokenString(t, numMaxTextWidth)
      arrLineBrokenText.append(arrResult[0])
      if len(arrResult)==1:
        boolLoop=False
      else:
        t=arrResult[1]

  ## Insert line breaks if necessary
  #############################################################

  #for thisText in arrText:
  for thisText in arrLineBrokenText:
    label = fontInfo.render(thisText, 1, colText)
    screen.blit(label, (x, y))
    y+=numInfoLineHeight


#----------------------------------------------------------------------------------------------


def PutTargetPixel(tx, ty, col):
  global screen
  global twtopx
  global twtopy
  size=numTargetScale
  if size<1:
    size=1
  pxRect=(twtopx+(tx*numTargetScale), twtopy+(ty*numTargetScale), size, size)
  screen.fill(col, pxRect)


#----------------------------------------------------------------------------------------------


def DrawBresenham(x0, y0, x1, y1, col, speed, boolSelectedElement=False, element=0, boolRender=True):
  # Wikipedia's "Optimization" routine
  # http://en.wikipedia.org/wiki/Bresenham's_line_algorithm#Optimization
  global arrPlots

  x0=int(x0)
  y0=int(y0)
  x1=int(x1)
  y1=int(y1)
  
  boolLocalDebug=False  # True  # False

  #############################################
  ## Determine line case

  ldx=x1-x0
  ldy=y1-y0
  if ldx==0 and ldy==0:
    numLineCase=0
  elif ldy==0:
    if ldx>0:
      numLineCase=9
    else:
      numLineCase=1
  elif ldx==0:
    if ldy>0:
      numLineCase=5
    else:
      numLineCase=13
  elif ldx>0:
    if ldy>0:
               # 6, 7, 8
      if abs(ldx)==abs(ldy):
        numLineCase=7
      if abs(ldx)>abs(ldy):
        numLineCase=8
      elif abs(ldx)<abs(ldy):
        numLineCase=6
    else:
               # 10, 11, 12
      if abs(ldx)==abs(ldy):
        numLineCase=11
      if abs(ldx)>abs(ldy):
        numLineCase=10
      elif abs(ldx)<abs(ldy):
        numLineCase=12
  else:
    if ldy>0:
               # 2, 3, 4
      if abs(ldx)==abs(ldy):
        numLineCase=3
      if abs(ldx)>abs(ldy):
        numLineCase=2
      elif abs(ldx)<abs(ldy):
        numLineCase=4
    else:
               # 14, 15, 16
      if abs(ldx)==abs(ldy):
        numLineCase=15
      if abs(ldx)>abs(ldy):
        numLineCase=16
      elif abs(ldx)<abs(ldy):
        numLineCase=14

  ## Determine line case
  #############################################

  # Plot
  arrPlotsTemp=[]
  boolYMajor=False
  
  boolXneg=True
  
  if abs(y1-y0)>abs(x1-x0):
    boolYMajor=True
    # swap x0 and y0
    temp=x0
    x0=y0
    y0=temp
    # swap x1 and y1
    temp=x1
    x1=y1
    y1=temp
    
  if x0>x1:
    # swap x0 and x1
    temp=x0
    x0=x1
    x1=temp
    boolXneg=False
    # swap y0 and y1
    temp=y0
    y0=y1
    y1=temp

  dx=x1-x0
  dy=abs(y1-y0)
  error=dx/2
  y=y0
  ystep=-1
  if y0<y1:
    ystep=1

  x=x0
  for n in range(dx+1):
    if boolYMajor:
      #PlotPixel(y, x)
      if boolRender:
        PutTargetPixel(y, x, col)
      if not boolSelectedElement:
        arrPlotsTemp.append((y, x, speed, element))
    else:
      #PlotPixel(x, y)
      if boolRender:
        PutTargetPixel(x, y, col)
      if not boolSelectedElement:
        arrPlotsTemp.append((x, y, speed, element))
    error-=dy
    if error<0:
      y+=ystep
      error+=dx
    x+=1
  if boolYMajor==False and boolXneg==False:
    arrPlotsTemp=list(reversed(arrPlotsTemp))
  if numLineCase==12 or numLineCase==13 or numLineCase==14:
    arrPlotsTemp=list(reversed(arrPlotsTemp))
  arrPlots.extend(arrPlotsTemp)
  if boolLocalDebug:
    print "# numLineCase: "+str(numLineCase)
    print "# boolXneg: "+str(boolXneg)
    print "# boolYMajor: "+str(boolYMajor)
    print "# ystep: "+str(ystep)
    print "# dy: "+str(dy)
    print "# dx: "+str(dx)
    print "# arrPlot Y values:"
    strO=""
    for p in arrPlotsTemp:
      strO+=str(p[1])+", "
    print "    # "+strO


###############################################################################################################
###############################################################################################################
## Cardinals


# Line case 1
# ------------
# numLineCase: 1
# boolXneg: False
# boolYMajor: False
# ystep: -1
# dy: 0
# dx: 10
# arrPlot X values:
# 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10,


# Line case 5
# ------------
# numLineCase: 5
# boolXneg: True
# boolYMajor: True
# ystep: -1
# dy: 0
# dx: 14
# arrPlot X values:
# 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,


# Line case 9
# ------------
# numLineCase: 9
# boolXneg: True
# boolYMajor: False
# ystep: -1
# dy: 0
# dx: 16
# arrPlot X values:
# 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,


# Line case 13
# -------------
# numLineCase: 13
# boolXneg: False
# boolYMajor: True
# ystep: -1
# dy: 0
# dx: 10
# arrPlot X values:
# 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,


# Line case 3
# ------------
# numLineCase: 3
# boolXneg: False
# boolYMajor: False
# ystep: -1
# dy: 5
# dx: 5
# arrPlot X values:
# 10, 9, 8, 7, 6, 5,


# Line case 7
# ------------
# numLineCase: 7
# boolXneg: True
# boolYMajor: False
# ystep: 1
# dy: 7
# dx: 7
# arrPlot X values:
# 10, 11, 12, 13, 14, 15, 16, 17,

# Line case 11
# -------------
# numLineCase: 11
# boolXneg: True
# boolYMajor: False
# ystep: -1
# dy: 7
# dx: 7
# arrPlot X values:
# 16, 17, 18, 19, 20, 21, 22, 23,


# Line case 15
# -------------
# numLineCase: 15
# boolXneg: False
# boolYMajor: False
# ystep: 1
# dy: 8
# dx: 8
# arrPlot X values:
# 28, 27, 26, 25, 24, 23, 22, 21, 20,


## Cardinals
###############################################################################################################
###############################################################################################################
## Y pos


# X neg, X major [2]
# ---------------
# numLineCase: 2
# boolXneg: False
# boolYMajor: False
# ystep: -1
# dy: 5
# dx: 16
# arrPlot X values:
# 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23,


# X neg, Y major [4]
# ---------------
# numLineCase: 4
# boolXneg: True
# boolYMajor: True
# ystep: -1
# dy: 2
# dx: 5
# arrPlot X values:
# 31, 31, 30, 30, 29, 29,


# X pos, Y major [6]
# ---------------
# numLineCase: 6
# boolXneg: True
# boolYMajor: True
# ystep: 1
# dy: 2
# dx: 7
# arrPlot X values:
# 25, 25, 26, 26, 26, 26, 27, 27,


# X pos, X major [8]
# ---------------
# numLineCase: 8
# boolXneg: True
# boolYMajor: False
# ystep: 1
# dy: 3
# dx: 12
# arrPlot X values:
# 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,


## Y pos
###############################################################################################################
###############################################################################################################
## Y neg


# X neg, X major [16]
# ---------------
# numLineCase: 16
# boolXneg: False
# boolYMajor: False
# ystep: 1
# dy: 2
# dx: 10
# arrPlot X values:
# 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27,


# X neg, Y major [14]
# ---------------
# numLineCase: 14
# boolXneg: False
# boolYMajor: True
# ystep: 1
# dy: 2
# dx: 7
# arrPlot X values:
# 22, 22, 21, 21, 21, 21, 20, 20,


# X pos, Y major [12]
# ---------------
# numLineCase: 12
# boolXneg: False
# boolYMajor: True
# ystep: -1
# dy: 1
# dx: 6
# arrPlot X values:
# 17, 17, 17, 18, 18, 18, 18,


# X pos, X major [10]
# ---------------
# numLineCase: 10
# boolXneg: True
# boolYMajor: False
# ystep: -1
# dy: 2
# dx: 6
# arrPlot X values:
# 22, 23, 24, 25, 26, 27, 28,


## Y neg
###############################################################################################################
###############################################################################################################


#----------------------------------------------------------------------------------------------


def DrawBezier(x0, y0, x1, y1, h0x, h0y, h1x, h1y, col, speed, boolSelectedElement=False, element=-1, boolRender=True):
#  C++
#for (t = 0.0; t < 1.0; t += 0.0005)
#    {
#	double xt = pow (1-t, 3) * x[0] + 3 * t * pow (1-t, 2) * x[1] +
#		    3 * pow (t, 2) * (1-t) * x[2] + pow (t, 3) * x[3];
# 
#	double yt = pow (1-t, 3) * y[0] + 3 * t * pow (1-t, 2) * y[1] +
#		    3 * pow (t, 2) * (1-t) * y[2] + pow (t, 3) * y[3];
# 
#	putpixel (xt, yt, WHITE);
#    }

  #global numBezierSteps
  
  if element!=-1:
    #print arrElements[element]
    if "bezier_steps" in arrElements[element].keys():
      numBezierSteps=arrElements[element]["bezier_steps"]
    else:
      arrElements[element]["bezier_steps"]=numDefaultBezierSteps
      numBezierSteps=arrElements[element]["bezier_steps"]

  boolLocalDebug=False  # old debugging
  boolLocalDebug2=False  # True  # False  # new debugging

  if boolLocalDebug2:
    print "#"*92
    print "#"*92
    print "#### DrawBezier, vertices: ("+str(x0)+", "+str(y0)+") - ("+str(x1)+", "+str(y1)+")"

  if boolLocalDebug:
    arrColors=[]
    arrColors.append((255, 0, 0))
    arrColors.append((0, 255, 0))
    arrColors.append((0, 0, 255))

  x0=float(x0)
  h0x=float(h0x)
  h1x=float(h1x)
  x1=float(x1)
  y0=float(y0)
  h0y=float(h0y)
  h1y=float(h1y)
  y1=float(y1)

  x=[]
  x.append(x0)
  x.append(h0x)
  x.append(h1x)
  x.append(x1)
  y=[]
  y.append(y0)
  y.append(h0y)
  y.append(h1y)
  y.append(y1)
  numSteps=numBezierSteps  # Basically bezier resolution
  t=0.0
  for i in range(numSteps):
    xt = (pow (1-t, 3) * x[0]) + (3 * t * pow (1-t, 2) * x[1]) + (3 * pow (t, 2) * (1-t) * x[2]) + (pow (t, 3) * x[3])
    yt = (pow (1-t, 3) * y[0]) + (3 * t * pow (1-t, 2) * y[1]) + (3 * pow (t, 2) * (1-t) * y[2]) + (pow (t, 3) * y[3])
    if i==0:
      prevX=xt
      prevY=yt
    else:
      if boolLocalDebug:
        colr=arrColors[i%len(arrColors)]
        DrawBresenham(prevX, prevY, xt, yt, colr, speed, boolSelectedElement, element, boolRender)
      else:
        if boolLocalDebug2:
          print "## DrawBresenham: ("+str(prevX)+", "+str(prevY)+"), ("+str(xt)+", "+str(yt)+")"
        DrawBresenham(prevX, prevY, xt, yt, col, speed, boolSelectedElement, element, boolRender)
      prevX=xt
      prevY=yt
    #PutTargetPixel(xt, yt, col)
    t+=1.0/numSteps
  if boolLocalDebug:
    i+=1
    colr=arrColors[i%len(arrColors)]
    #DrawBresenham(x1, y1, xt, yt, colr, speed, boolSelectedElement, element)
    DrawBresenham(xt, yt, x1, y1, colr, speed, boolSelectedElement, element, boolRender)
  else:
    #DrawBresenham(x1, y1, xt, yt, col, speed, boolSelectedElement, element)
    DrawBresenham(xt, yt, x1, y1, col, speed, boolSelectedElement, element, boolRender)


#----------------------------------------------------------------------------------------------


def DrawHandle(x, y, type, selected=False):
  global numSquareHandleSize
  global numCircleHandleRadius
  global numHandleLineWidth
  size=numSquareHandleSize
  radius=numCircleHandleRadius
  width=numHandleLineWidth
  hs=size/2.0
  if type==HANDLE_TYPE_VERTEX:
    thisCol=colHandleLineEnd
    if selected:
      thisCol=colHandleLineEndSelected
    x*=numTargetScale
    y*=numTargetScale
    x+=twtopx+(numTargetScale/2.0)
    y+=twtopy+(numTargetScale/2.0)
    rectHandle=(x-hs, y-hs, size, size)
    rectShadow=(x-hs+2, y-hs+2, size, size)
    pygame.draw.rect(screen, colHandleShadow, rectShadow, width)
    pygame.draw.rect(screen, thisCol, rectHandle, width)
  elif type==HANDLE_TYPE_CURVE:
    thisCol=colHandleCurve
    if selected:
      thisCol=colHandleCurveSelected
    x*=numTargetScale
    y*=numTargetScale
    x+=twtopx+(numTargetScale/2.0)
    y+=twtopy+(numTargetScale/2.0)
    x=int(x)
    y=int(y)
    pygame.draw.circle(screen, colHandleShadow, (x+2, y+2), radius, width)
    pygame.draw.circle(screen, thisCol, (x,y), radius, width)


#----------------------------------------------------------------------------------------------


def GetAngle(p0, p1):
  dy=float(p1[1])-float(p0[1])
  dy=-dy
  dx=float(p1[0])-float(p0[0])
  theta=math.atan2(dy, dx)
  return theta


#----------------------------------------------------------------------------------------------


def GetDistance(p1, p2):
  x0=p1[0]
  y0=p1[1]
  x1=p2[0]
  y1=p2[1]
  a=float(x1)-float(x0)
  b=float(y1)-float(y0)
  c=(a*a)+(b*b)
  return math.sqrt(c)


#----------------------------------------------------------------------------------------------


def BuildPlotPreviewPath():
  global arrPlots
  global arrOutPlots

  boolLimitLoops=False
  numLimitLoopsTo=200

  boolLocalDebug=False  # True
  
  if not arrPlots:
    return 0

  if boolLocalDebug: print "@"*92
  if boolLocalDebug: print "@"*92

  arrTemp=copy.deepcopy(arrPlots)
  arrPlots=[]

  p=arrTemp[0]
  prevP=(p[0], p[1])
  numAbsDist=0.0
  idx=0
  for i,p in enumerate(arrTemp):
    thisPlot={}
    thisPlot["index"]=idx
    thisPlot["xy"]=(p[0], p[1])
    thisPlot["speed"]=p[2]
    thisPlot["element_id"]=p[3]
    d=GetDistance(prevP, thisPlot["xy"])
    thisPlot["distance"]=d
    if thisPlot["xy"]!=prevP or i==0:
      numAbsDist+=d
      #print str(i)+":  dist: "+str(numAbsDist)+", speed: "+str(thisPlot["speed"])
      thisPlot["absolute_distance"]=numAbsDist
      arrPlots.append(thisPlot)
      idx+=1
    prevP=thisPlot["xy"]
  if boolLocalDebug: print "\nBuilt updated arrPlots list, total length of path: "+str(numAbsDist)+", last index: "+str(idx-1)
  if boolLocalDebug: print "\n"+"*"*78+"\n"

  if boolLocalDebug:
    print "arrPlots:"
    for i,p in enumerate(arrPlots):
      print "  "+str(i).zfill(4)+": "+str(p["xy"])+", speed: "+str(p["speed"])+", id: "+str(p["element_id"])
    print


  if boolInterpolateSpeedAcrossElements:
    ###########################################################
    ## Modify path to interpolate speed across each element

    if boolLocalDebug: print "\n"+"-"*78+"\n"
    if boolLocalDebug: print "\nIndices:"
    idx=len(arrPlots)-1
    arrVertexIndices=[]
    prevId=-999
    for i in range(len(arrPlots)):
      p=arrPlots[idx]
      thisId=p["element_id"]
      if boolLocalDebug: strOut=str(idx).zfill(4)+": thisId: "+str(thisId)+", prevId: "+str(prevId)
      if thisId!=prevId:
        arrVertexIndices.append(idx)
        if boolLocalDebug: strOut+="  **Boing**"
      #if boolLocalDebug: print strOut
      prevId=p["element_id"]
      idx-=1

    arrVertexIndices=list(reversed(arrVertexIndices))

    if boolLocalDebug: print "\n"+"-"*78+"\n"


    if boolLocalDebug: 
      print "Indices:"
      for i,n in enumerate(arrVertexIndices):
        print "  "+str(i)+": "+str(arrPlots[n]["speed"])+", n: "+str(n)
      print

    # Interpolate
    for i in range(len(arrVertexIndices)-1):
      vi=arrVertexIndices[i]
      vinext=arrVertexIndices[i+1]
      startix=vi
      startspeed=arrPlots[startix]["speed"]
      endix=vinext
      endspeed=arrPlots[endix]["speed"]
      numLen=endix-startix
      deltaspeed=endspeed-startspeed
      speed=startspeed
      speedstep=deltaspeed/float(numLen)
      for n in range(numLen):
        arrPlots[startix+n]["speed"]=speed
        if boolLocalDebug: print "  "+str(speed)+" px/vbl"
        speed+=speedstep

    ## Modify path to interpolate speed across each element
    ###########################################################

  n=0
  pos=0.0
  arrOutPlots=[]
  start=arrPlots[n]
  startpoint=start["absolute_distance"]
  speed=start["speed"]
  end=arrPlots[n+1]
  endpoint=end["absolute_distance"]

  numLoops=0
  boolLoop=True
  while boolLoop:
    if boolLocalDebug: print "pos: "+str(pos)
    startdist=abs(startpoint-pos)
    enddist=abs(endpoint-pos)
    if boolLocalDebug: print "  startdist: "+str(startdist)
    if boolLocalDebug: print "  enddist: "+str(enddist)
    if startdist<enddist:
      arrOutPlots.append(start)
      written=start
    else:
      arrOutPlots.append(end)
      written=end
    if boolLocalDebug: print "  ** Written: "+str(written["index"])+", absdist: "+str(written["absolute_distance"])

    pos+=speed

    boolOutOfData=False
    if pos>=endpoint:
      # Find new start- and endpoints
      if boolLocalDebug: print "  Looking for new start, end, pos: "+str(pos)+", startpoint: "+str(startpoint)+", endpoint: "+str(endpoint)
      numInnerLoops=0
      while pos>endpoint:
        #print "len(arrPlots): "+str(len(arrPlots))+", n: "+str(n)
        if len(arrPlots)>n+2:
          n+=1
          start=arrPlots[n]
          startpoint=start["absolute_distance"]
          speed=start["speed"]
          end=arrPlots[n+1]
          endpoint=end["absolute_distance"]
          if boolLocalDebug: print "    new candidate for start, end: "+str(startpoint)+", "+str(endpoint)
        else:
          #Fatal("Out of data in innerloop")
          boolOutOfData=True
          break

        numInnerLoops+=1
        if boolLimitLoops:
          if numInnerLoops>numLimitLoopsTo:
            Fatal("numInnerLoops: "+str(numInnerLoops))

    if boolOutOfData:
      boolLoop=False

    numLoops+=1
    if boolLimitLoops:
      if numLoops>numLimitLoopsTo:
        Fatal("numLoops: "+str(numLoops))

  if boolLocalDebug: print "All done in "+str(numLoops)+" iterations!"


#----------------------------------------------------------------------------------------------


def AddNumKey(key, val):
  global arrNumKeysValues
  global arrNumKeys

  arrNumKeys.append(key)
  thisKey={}
  thisKey["key"]=key
  thisKey["value"]=float(val)
  arrNumKeysValues.append(thisKey)


#----------------------------------------------------------------------------------------------


def MetaAddDoubleNumKey(key1, key2, val):
  global arrNumKeysValues

  AddNumKey(key1, val)
  AddNumKey(key2, val)


#----------------------------------------------------------------------------------------------


def Init():
  global arrNumKeysValues

  MetaAddDoubleNumKey(K_0, K_KP0, 0)
  MetaAddDoubleNumKey(K_1, K_KP0, 1)
  MetaAddDoubleNumKey(K_2, K_KP0, 2)
  MetaAddDoubleNumKey(K_3, K_KP0, 3)
  MetaAddDoubleNumKey(K_4, K_KP0, 4)
  MetaAddDoubleNumKey(K_5, K_KP0, 5)
  MetaAddDoubleNumKey(K_6, K_KP0, 6)
  MetaAddDoubleNumKey(K_7, K_KP0, 7)
  MetaAddDoubleNumKey(K_8, K_KP0, 8)
  MetaAddDoubleNumKey(K_9, K_KP0, 9)

#  for thisKey in arrNumKeysValues:
#    print "key: "+str(thisKey["key"])+", val: "+str(thisKey["value"])
#  exit(0)


#----------------------------------------------------------------------------------------------


def LoadFile(strFilename):
  global strLatestPathFilename
  global strLatestExportFilename
  global arrElements
  global numCurrentElement
  global numTargetScreenWidth
  global numTargetScreenHeight
  global numPlaybackFrameRate

  strLatestPathFilename=strFilename
  strLatestExportFilename=strFilename
  if xia.EndsWith(strLatestExportFilename, ".path"):
    strLatestExportFilename=strLatestExportFilename[:-len(".path")]
  arrJSON=xia.GetTextFile(strFilename)
  strJSON="".join(arrJSON)
  arrElements="--"
  arrJSON=json.loads(strJSON)
  arrElements=arrJSON["elements"]
  numCurrentElement=arrJSON["current_element"]
  if "target_screen_width" in arrJSON.keys():
    numTargetScreenWidth=arrJSON["target_screen_width"]
  if "target_screen_height" in arrJSON.keys():
    numTargetScreenHeight=arrJSON["target_screen_height"]
  if "framerate" in arrJSON.keys():
    numPlaybackFrameRate=arrJSON["framerate"]
  #print arrJSON
  ###############################################################
  ## Adjust for missing features on element level

  for i in range(len(arrElements)):
    if not "bezier_steps" in arrElements[i].keys():
      arrElements[i]["bezier_steps"]=numDefaultBezierSteps

  ## Adjust for missing features on element level
  ###############################################################                


#----------------------------------------------------------------------------------------------


def BuildExportData(arrOutPlots, boolCullX=False, boolCullY=False, numPointsPerLine=14):
  boolOutOfRange=False
  arrExportData=[]
  numByteMultiplier=4
  numValPairs=len(arrOutPlots)
  numBytes=numValPairs*numByteMultiplier
  if boolCullX or boolCullY:
    numByteMultiplier=2
    numVals=len(arrOutPlots)
    numBytes=numVals*numByteMultiplier
  strVals="  ; "+str(numValPairs)+" pairs of coords"
  if boolCullX:
    strVals="  ; "+str(numVals)+" Y-coords"
  elif boolCullY:
    strVals="  ; "+str(numVals)+" X-coords"
  strVals+=", totalling "+str(numBytes)+" bytes"
  strLabels="  ;    X,   Y"
  if boolCullX:
    strLabels="  ;    Y"
  elif boolCullY:
    strLabels="  ;    X"
  arrExportData.append("  ;---------------------------------------------------------------------------")
  arrExportData.append("  ;-- Path data exported using Pathologist v"+strVersion+" by XiA")
  arrExportData.append("  ;---------------------------------------------------------------------------")
  arrExportData.append(strVals)
  arrExportData.append("  ;---------------------------------------------------------------------------")
  arrExportData.append("  ;-- Path")
  arrExportData.append("")
  arrExportData.append(strLabels)
  numVals=0
  strOut="  dc.w "
  for p in arrOutPlots:
    x=p["xy"][0]
    y=p["xy"][1]
    if numVals>=numPointsPerLine:
      # Output string, create new one
      # cull ", " from end of line, if it's there
      strOut+="*"
      strOut=strOut.replace(", *", "")
      arrExportData.append(strOut)
      numVals=0
      strOut="  dc.w "
    if x>32767 or x<-32768:
      boolOutOfRange=True
      break
    if y>32767 or y<-32768:
      boolOutOfRange=True
      break
    #strLine="  dc.w "+str(x)+", "+str(y)
    if boolCullX:
      #strLine="  dc.w "+str(y)
      strOut+=str(y)+", "
      numVals+=1
    elif boolCullY:
      #strLine="  dc.w "+str(x)
      strOut+=str(x)+", "
      numVals+=1
    else:
      strOut+=str(x)+", "+str(y)+", "
      numVals+=2
    #arrExportData.append(strLine)
  # Finish off data
  if numVals>0:
    # Output string, create new one
    # cull ", " from end of line, if it's there
    strOut+="*"
    strOut=strOut.replace(", *", "")
    arrExportData.append(strOut)

  arrExportData.append("")
  arrExportData.append("  ;-- Path")
  arrExportData.append("  ;---------------------------------------------------------------------------")
  arrOut={}
  if boolOutOfRange:
    arrOut["success"]=False
    return arrOut
  arrOut["success"]=True
  arrOut["data"]=arrExportData
  return arrOut


#----------------------------------------------------------------------------------------------


def RenderLinesAndBeziers(boolRender=True):
  global arrElements
  if arrElements:
    for i in range(len(arrElements)):
      thisElement=arrElements[i]
      if thisElement["type"]==ELEMENT_TYPE_START:
        numPrevX=thisElement["coords"][0]
        numPrevY=thisElement["coords"][1]
        numSpeed=thisElement["speed"]
        DrawBresenham(numPrevX, numPrevY, numPrevX, numPrevY, colLineRegular, numSpeed, False, i, boolRender)
      if thisElement["type"]==ELEMENT_TYPE_LINE:
        arrCoords=thisElement["coords"]
        numSpeed=thisElement["speed"]
        DrawBresenham(numPrevX, numPrevY, arrCoords[0], arrCoords[1], colLineRegular, numSpeed, False, i, boolRender)
        numPrevX=arrCoords[0]
        numPrevY=arrCoords[1]
      if thisElement["type"]==ELEMENT_TYPE_BEZIER:
        prevElement=arrElements[i-1]
        numX0=prevElement["coords"][0]
        numY0=prevElement["coords"][1]
        numX1=thisElement["coords"][0]
        numY1=thisElement["coords"][1]
        c=thisElement["coords"]
        numSpeed=thisElement["speed"]
        DrawBezier(numPrevX, numPrevY, c[0], c[1], c[2]+numX0, c[3]+numY0, c[4]+numX1, c[5]+numY1, colLineRegular, numSpeed, False, i, boolRender)
        numPrevX=c[0]
        numPrevY=c[1]


## Functions
################################################################################################
################################################################################################


if boolDebug:
  print "Importing libraries...",
import sys
import os
import subprocess
#from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy
import random
import math

#sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out
#
#import pdb

import json

from Tkinter import *
import Tkinter, Tkconstants, tkFileDialog
 
import pygame
from pygame.locals import *


if boolDebug:
  print "done\n"


################################################################################################
################################################################################################
## Test code


if 1==2:
  
  print str(pygame.display.info())  
  exit(0)

  
  arrA=[]
  arrA.append({"letter": "a", "number": 4})
  arrA.append({"letter": "b", "number": 9})
  arrA.append({"letter": "c", "number": 3})
  arrA.append({"letter": "d", "number": 7})
  arrA.append({"letter": "e", "number": 6})
  arrA.append({"letter": "f", "number": 0})

  for i,a in enumerate(arrA):
    print str(i)+": "+str(a)

  print "\n"+"-"*88+"\n"

  InsertMe={"letter": "X", "number": 3.14}
  arrA.insert(3, InsertMe)

  for i,a in enumerate(arrA):
    print str(i)+": "+str(a)
  
  exit(0)


## Test code
################################################################################################
################################################################################################
## Handle input parameters


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help=".path file stub to load",
                       metavar="FILE",
                       required=False)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help=".s file to export to",
                       metavar="FILE",
                       required=False)

objParser.add_argument("-nx", "--no-x",
                       dest="no-x",
                       help="don't output X values",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-ny", "--no-y",
                       dest="no-y",
                       help="don't output Y values",
                       action="store_const",
                       const=True,
                       required=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""
boolVerboseMode=False

boolCullXValues=False
boolCullYValues=False

numArgs=0
Args=vars(objParser.parse_args())


for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=xia.StrToBool(strValue)

  elif key=="no-x" and strValue!="None":
    boolCullXValues=xia.StrToBool(strValue)
  elif key=="no-y" and strValue!="None":
    boolCullYValues=xia.StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)

if boolCullXValues and boolCullYValues:
  Fatal("Both --no-x and --no-y parameters set, which would result in no output!")


# Test infile
if boolInFileGiven:
  numResult=xia.CheckFileExistsAndReadable(strInFile)
  if numResult!=0:
    if numResult&xia.FILE_EXISTS_FAIL:
      Fatal("Can't find file \""+strInFile+"\"!")
    if numResult&xia.FILE_READABLE_FAIL:
      Fatal("Can't read file \""+strInFile+"\"!")


# Export without opening window
if boolInFileGiven:
  strFilename=strInFile

  boolFileLoadedOk=True
  # Test infile
  numResult=xia.CheckFileExistsAndReadable(strFilename)
  if numResult!=0:
    if numResult&xia.FILE_EXISTS_FAIL:
      Fatal("Error: Can't find file \""+strFilename+"\"!")
      boolFileLoadedOk=False
    if numResult&xia.FILE_READABLE_FAIL:
      Fatal("Error: Can't read file \""+strFilename+"\"!")
      boolFileLoadedOk=False
  if boolFileLoadedOk:
    LoadFile(strFilename)
    print "Opened file \""+strFilename+"\"."

    RenderLinesAndBeziers(False)
    BuildPlotPreviewPath()
    if not arrOutPlots:
      Fatal("No data to export!")
    else:
      boolOutOfRange=False
      arrResult=BuildExportData(arrOutPlots, boolCullXValues, boolCullYValues) ## number of values per line as last arg here, get from args
      if arrResult["success"]==False:
        boolOutOfRange=False
      else:
        arrExportData=arrResult["data"]

      if boolOutOfRange:
        Fatal("Some data is outside word range (-32768 -- 32767) - export cancelled!")
      else:
        if boolOutFileGiven:
          strFilename=strOutFile
        else:
          if xia.EndsWith(strFilename, ".path"):
            strFilename=strFilename[:-len(".path")]
          if not xia.EndsWith(strFilename, ".s"):
            strFilename+=".s"
          Notice("No output filename given, output will be saved as \""+strFilename+"\".")
        xia.WriteTextFile(arrExportData, strFilename)
        print "Data exported to \""+strFilename+"\"."

  exit(0)


## Handle input parameters
################################################################################################
################################################################################################
## Tkinter


tkRoot=None


#----------------------------------------------------------------------------------------------


def InitTk():
  global tkRoot
  tkRoot=Tk()  # Create root window
  tkRoot.withdraw()  # Hide the god ugly root window


#----------------------------------------------------------------------------------------------


def GetExportFileName(strFile=""):
  arrFileTypes=[]
  arrFileTypes.append(('.s files', '.s'))
  arrFileTypes.append(('all files', '.*'))
  #arrFileTypes.append(('text files', '.txt'))
  strFilename=tkFileDialog.asksaveasfilename(parent=tkRoot,
                                             initialdir=os.getcwd(),
                                             initialfile=strFile,
                                             title="Please select a filename for exporting:",
                                             filetypes=arrFileTypes)
  return strFilename


#----------------------------------------------------------------------------------------------


def GetSaveFileName(strFile=""):
  arrFileTypes=[]
  arrFileTypes.append(('.path files', '.path'))
  arrFileTypes.append(('all files', '.*'))
  #arrFileTypes.append(('text files', '.txt'))
  strFilename=tkFileDialog.asksaveasfilename(parent=tkRoot,
                                             initialdir=os.getcwd(),
                                             initialfile=strFile,
                                             title="Please select a filename for saving:",
                                             filetypes=arrFileTypes)
  return strFilename


#----------------------------------------------------------------------------------------------


def GetOpenFileName():
  arrFileTypes=[]
  arrFileTypes.append(('.path files', '.path'))
  arrFileTypes.append(('all files', '.*'))
  #arrFileTypes.append(('text files', '.txt'))
  strFilename=tkFileDialog.askopenfilename(parent=tkRoot,
                                           initialdir=os.getcwd(),
                                           title="Please select a .path file:",
                                           filetypes=arrFileTypes)
  return strFilename


## Tkinter
################################################################################################
################################################################################################
## Main loop


def main():
  global numScreenWidth
  global numScreenHeight
  global numWindowWidth
  global numWindowHeight
  global screen
  global myClock
  global fontInfo
  global twtopx
  global twtopy
  global arrElements
  global numMode
  global numStartX
  global numStartY
  global numCurrentElement
  global dummyElement
  global numCurrentHandle
  global strWaitingForClickMessage
  global numTargetScale
  global numTargetScaleIndex
  global numSquareHandleSize
  global numCircleHandleRadius
  global numHandleLineWidth
  global numEditedSpeed
  global numFrameRate
  global arrPlots
  global arrNumKeysValues
  global arrNumKeys
  global numTargetScreenWidth
  global numTargetScreenHeight
  global numAnimFrame
  global numPlaybackFrameRate
  global strLatestPathFilename
  global strLatestExportFilename


  Init()

  InitTk()

  numScreenWidth=tkRoot.winfo_screenwidth()
  numScreenHeight=tkRoot.winfo_screenheight()
  numWindowWidth=int(numScreenWidth*0.9)
  numWindowHeight=int(numScreenHeight-110)

  #os.environ['SDL_VIDEO_CENTERED']="1"
  os.environ['SDL_VIDEO_WINDOW_POS'] = '%i,%i' % (40, 40)

#  stdout=sys.__stdout__
#  stderr=sys.__stderr__
#  sys.stdout=open(os.devnull,'w')
#  sys.stderr=open(os.devnull,'w')
  pygame.init()
#  sys.stdout=stdout
#  sys.stderr=stderr

#  # Load and set logo
#  logo = pygame.image.load(strDataFolder+os.sep+"logo.png")
#  pygame.display.set_icon(logo)
  pygame.display.set_caption("Pathologist.py v"+strVersion)
  screen = pygame.display.set_mode((numWindowWidth,numWindowHeight), HWSURFACE|DOUBLEBUF|RESIZABLE)
  fontInfo=pygame.font.SysFont("Calibri", numInfoFontSize, bold=True)
  pygame.mouse.set_visible(True)
  myClock=pygame.time.Clock()


  boolRunning = True

  # Main loop
  while boolRunning:
    
    # Some coords math
    cx=numWindowWidth/2
    cy=numWindowHeight/2    
    tw=(numTargetScreenWidth*numTargetScale)
    th=(numTargetScreenHeight*numTargetScale)
    bs=numTargetBorderSize
    twtopx=cx-(tw/2)  # target window top X
    twtopy=cy-(th/2)  # target window top Y
    twtopy+=(numInfoFontSize+numInfoPositionY)/2

    # Get mouse pos and convert to target coords
    m=pygame.mouse.get_pos()
    numMouseX=m[0]
    numMouseY=m[1]
    numTargetMouseX=numMouseX-twtopx
    numTargetMouseY=numMouseY-twtopy
    numTargetMouseX/=numTargetScale
    numTargetMouseY/=numTargetScale
    numTargetMouseX=int(numTargetMouseX)
    numTargetMouseY=int(numTargetMouseY)

    numKeyboardModifiers=pygame.key.get_mods()
    for event in pygame.event.get():

      if event.type==VIDEORESIZE:
        arrNewSize=event.dict['size']
        numWindowWidth=arrNewSize[0]
        numWindowHeight=arrNewSize[1]
        screen=pygame.display.set_mode( (numWindowWidth, numWindowHeight) , HWSURFACE|DOUBLEBUF|RESIZABLE)

        #######################################################################
        ## Recalculate stuff related to target window and the scaling thereof

        cx=numWindowWidth/2
        cy=numWindowHeight/2

        # If Targetscreen is too large for new window size, scale it down
        if tw>numWindowWidth or th>numWindowHeight:
          while (tw>numWindowWidth or th>numWindowHeight) and numTargetScaleIndex<len(arrTargetScales)-1:
            if numTargetScaleIndex<len(arrTargetScales)-1:
              numTargetScaleIndex+=1
              numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
            tw=(numTargetScreenWidth*numTargetScale)
            th=(numTargetScreenHeight*numTargetScale)
        tw=(numTargetScreenWidth*numTargetScale)
        th=(numTargetScreenHeight*numTargetScale)
        bs=numTargetBorderSize
        twtopx=cx-(tw/2)  # target window top X
        twtopy=cy-(th/2)  # target window top Y
        twtopy+=(numInfoFontSize+numInfoPositionY)/2


        numTargetMouseX=numMouseX-twtopx
        numTargetMouseY=numMouseY-twtopy
        numTargetMouseX/=numTargetScale
        numTargetMouseY/=numTargetScale
        numTargetMouseX=int(numTargetMouseX)
        numTargetMouseY=int(numTargetMouseY)

        ## Recalculate stuff related to target window and the scaling thereof
        #######################################################################


      if event.type==pygame.QUIT:
        numMode=EDITOR_MODE_QUIT_ARE_YOU_SURE

      boolMouseClick=False
      if event.type==MOUSEBUTTONDOWN:
        if event.button==4:
          if numTargetScaleIndex>0:
            numTargetScaleIndex-=1
            numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
        elif event.button==5:
          if numTargetScaleIndex<len(arrTargetScales)-1:
            numTargetScaleIndex+=1
            numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
        else:
          boolMouseClick=True

      if numMode==EDITOR_MODE_PLACE_LINE_START:
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE:
            numMode=EDITOR_MODE_NORMAL
        if boolMouseClick:
          numStartX=numTargetMouseX
          numStartY=numTargetMouseY
          numMode=EDITOR_MODE_PLACE_LINE_END

      elif numMode==EDITOR_MODE_PLACE_LINE_END:
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE:
            numMode=EDITOR_MODE_NORMAL
        if boolMouseClick:
          thisElement={}
          arrCoords=[numStartX, numStartY]
          thisElement["coords"]=arrCoords
          thisElement["type"]=ELEMENT_TYPE_START
          thisElement["speed"]=numDefaultSpeed
          arrElements.append(thisElement)
          thisElement={}
          arrCoords=[numTargetMouseX, numTargetMouseY]
          thisElement["coords"]=arrCoords
          thisElement["type"]=ELEMENT_TYPE_LINE
          thisElement["speed"]=numDefaultSpeed
          arrElements.append(thisElement)
          numCurrentElement=len(arrElements)-1  # The one we just created
          numMode=EDITOR_MODE_NORMAL

      elif numMode==EDITOR_MODE_VERTEX_GRAB:
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE:
            numMode=EDITOR_MODE_NORMAL
        if boolMouseClick:
          numMode=EDITOR_MODE_NORMAL
        if numCurrentElement!=None:
          if numCurrentHandle!=None:
            thisElement=arrElements[numCurrentElement]
            if thisElement["type"]==ELEMENT_TYPE_BEZIER:
              prevElement=arrElements[numCurrentElement-1]
              numX0=prevElement["coords"][0]
              numY0=prevElement["coords"][1]
              numX1=thisElement["coords"][0]
              numY1=thisElement["coords"][1]
              numReverseX=numTargetMouseX
              numReverseY=numTargetMouseY
              if numCurrentHandle==HANDLE_ID_OTHER_END_CURVE:
                # Previous vertex curve handle
                thisElement["coords"][2]=numReverseX-numX0
                thisElement["coords"][3]=numReverseY-numY0
              elif numCurrentHandle==HANDLE_ID_THIS_END_CURVE:
                # Current vertex curve handle
                thisElement["coords"][4]=numReverseX-numX1
                thisElement["coords"][5]=numReverseY-numY1
              elif numCurrentHandle==HANDLE_ID_VERTEX:
                # Current vertex handle
                thisElement["coords"][0]=numTargetMouseX
                thisElement["coords"][1]=numTargetMouseY
            elif thisElement["type"]==ELEMENT_TYPE_LINE:
              thisElement["coords"][0]=numTargetMouseX
              thisElement["coords"][1]=numTargetMouseY
            elif thisElement["type"]==ELEMENT_TYPE_START:
              thisElement["coords"][0]=numTargetMouseX
              thisElement["coords"][1]=numTargetMouseY

      elif numMode==EDITOR_MODE_WAIT_FOR_CLICK:
        if event.type==KEYDOWN:
          if event.key==K_SPACE:
            numMode=EDITOR_MODE_NORMAL
        if boolMouseClick:
          numMode=EDITOR_MODE_NORMAL

      elif numMode==EDITOR_MODE_SET_SPEED:
        numDelta=1.0
        if numKeyboardModifiers&(KMOD_SHIFT):
          numDelta=0.1
        elif numKeyboardModifiers&(KMOD_CTRL):
          numDelta=0.01
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE:
            numMode=EDITOR_MODE_NORMAL
          elif event.key==K_RETURN or event.key==K_KP_ENTER:
            for e in range(len(arrElements)):
              arrElements[e]["speed"]=numEditedSpeed
              numMode=EDITOR_MODE_NORMAL
          elif event.key==K_r:
            numEditedSpeed=round(numEditedSpeed)
          elif event.key==K_UP:
            numEditedSpeed+=numDelta
          elif event.key==K_DOWN:
            numEditedSpeed-=numDelta
          for thisKey in arrNumKeysValues:
            if event.key==thisKey["key"]:
              #print str(thisKey)
              numEditedSpeed=thisKey["value"]
        if numEditedSpeed<numMinimumSpeed:
          numEditedSpeed=numMinimumSpeed

      elif numMode==EDITOR_MODE_ANIMATED_PREVIEW:
        if event.type==KEYDOWN:
          if event.key==K_UP:
            if numFrameRate<150:
              numFrameRate+=10
              numPlaybackFrameRate=numFrameRate
          elif event.key==K_DOWN:
            if numFrameRate>10:
              numFrameRate-=10
              numPlaybackFrameRate=numFrameRate
          elif event.key==K_ESCAPE or event.key==K_a:
            numFrameRate=numEditorFrameRate
            numMode=EDITOR_MODE_NORMAL        

      elif numMode==EDITOR_MODE_PLOT_PREVIEW:
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE or event.key==K_p:
            numFrameRate=numEditorFrameRate
            numMode=EDITOR_MODE_NORMAL        

      elif numMode==EDITOR_MODE_QUIT_ARE_YOU_SURE:
        if event.type==KEYDOWN:
          if event.key==K_y:
            boolRunning=False
          if event.key==K_n:
            numMode=EDITOR_MODE_NORMAL

      elif numMode==EDITOR_MODE_NORMAL:
        if boolMouseClick:
          if numKeyboardModifiers&(KMOD_CTRL):
            ###################################################
            ## New element

            if len(arrElements)==0:
              thisElement={}
              arrCoords=[numTargetMouseX, numTargetMouseY]
              thisElement["coords"]=arrCoords
              thisElement["type"]=ELEMENT_TYPE_START
              thisElement["speed"]=numDefaultSpeed
              thisElement["bezier_steps"]=numDefaultBezierSteps
              arrElements.append(thisElement)
            else:
              # Fetch last element's speed
              numSpeed=arrElements[-1]["speed"]
              thisElement={}
              arrCoords=[numTargetMouseX, numTargetMouseY]
              thisElement["coords"]=arrCoords
              thisElement["type"]=ELEMENT_TYPE_LINE
              thisElement["speed"]=numSpeed
              thisElement["bezier_steps"]=numDefaultBezierSteps
              arrElements.append(thisElement)
            numCurrentElement=len(arrElements)-1  # The one we just created

            ## New element
            ###################################################
          else:
            # Click without CTRL
            ###################################################
            ## Grab handle
            if arrHandles:
              s=(numSquareHandleSize/numTargetScale)/2
              for h in arrHandles:
                numX=h["x"]
                numY=h["y"]
                boolHit=True
                if numTargetMouseX<numX-s:
                  boolHit=False
                if numTargetMouseX>numX+s:
                  boolHit=False
                if numTargetMouseY<numY-s:
                  boolHit=False
                if numTargetMouseY>numY+s:
                  boolHit=False
                if boolHit:
                  numCurrentElement=h["element"]
                  numCurrentHandle=h["handle_id"]
                  numMode=EDITOR_MODE_VERTEX_GRAB
                  break
            ## Grab handle
            ###################################################
        if event.type==KEYDOWN:
          if event.key==K_ESCAPE:
            numMode=EDITOR_MODE_QUIT_ARE_YOU_SURE
          elif event.key==K_SPACE and boolSpaceToDebugStuff:
            print
            print "Elements"
            n=0
            for e in arrElements:
              print "    "+str(n)+": "+TypeToString(e["type"])
              c=0
              for g in range(len(e["coords"])/2):
                print "        "+str(c)+": ("+str(e["coords"][c])+", "+str(e["coords"][c+1])+")"
                c+=2
              n+=1
            if arrHandles:
              print
              print "Handles"
              n=0
              for h in arrHandles:
                print "   "+str(n)+": type: "+HandleTypeToString(h["type"])+" ("+str(h["x"])+", "+str(h["y"])+") sel="+str(h["selected"])
                n+=1
          elif event.key==K_i:
            # Subdivide current element
            if numCurrentElement>0:
              x0=arrElements[numCurrentElement-1]["coords"][0]
              y0=arrElements[numCurrentElement-1]["coords"][1]
              x1=arrElements[numCurrentElement]["coords"][0]
              y1=arrElements[numCurrentElement]["coords"][1]
              #print "0: ("+str(x0)+", "+str(y0)+"), 1: ("+str(x1)+", "+str(y1)+")"
              dx=(x1-x0)/2
              dy=(y1-y0)/2
              x0+=dx
              y0+=dy
              #print str(arrElements[numCurrentElement])
              newElement={}
              newElement["coords"]=[x0, y0]
              newElement["type"]=arrElements[numCurrentElement]["type"]
              if newElement["type"]==ELEMENT_TYPE_BEZIER:
                newElement["coords"].extend([dx/3, dy/3])
                newElement["coords"].extend([-dx/3, -dy/3])
              newElement["speed"]=arrElements[numCurrentElement]["speed"]
              arrElements.insert(numCurrentElement, newElement)
          elif event.key==K_p:
            if len(arrElements)>1:
              # Plot preview mode
              BuildPlotPreviewPath()
              numFrameRate=numPlaybackFrameRate
              numMode=EDITOR_MODE_PLOT_PREVIEW
          elif event.key==K_a:
            if len(arrElements)>1:
              # Plot preview mode
              BuildPlotPreviewPath()
              numFrameRate=numPlaybackFrameRate
              numAnimFrame=0
              numMode=EDITOR_MODE_ANIMATED_PREVIEW
          elif event.key==K_v:
            # Set speed for all elements
            if arrElements:
              numEditedSpeed=arrElements[numCurrentElement]["speed"]
              numMode=EDITOR_MODE_SET_SPEED

          #######################################
          ## Edit speed

          elif event.key==K_r or event.key==K_UP or event.key==K_DOWN or event.key in arrNumKeys:
            numSpeed=arrElements[numCurrentElement]["speed"]
            numDelta=1.0
            if numKeyboardModifiers&(KMOD_SHIFT):
              numDelta=0.1
            elif numKeyboardModifiers&(KMOD_CTRL):
              numDelta=0.01
            if event.key==K_r:
              numSpeed=round(numSpeed)
            if event.key==K_UP:
              numSpeed+=numDelta
            elif event.key==K_DOWN:
              numSpeed-=numDelta
            for thisKey in arrNumKeysValues:
              if event.key==thisKey["key"]:
                #print str(thisKey)
                numSpeed=thisKey["value"]
            if numSpeed<numMinimumSpeed:
              numSpeed=numMinimumSpeed
            arrElements[numCurrentElement]["speed"]=numSpeed

          ## Edit speed
          #######################################


          elif event.key==K_DELETE or event.key==K_x:
            if arrElements:
              del arrElements[numCurrentElement]
              if arrElements:
                arrElements[0]["type"]=ELEMENT_TYPE_START
              if numCurrentElement>len(arrElements)-1:
                numCurrentElement-=1
          elif event.key==K_b or event.key==K_c:
            if arrElements:
              if arrElements[numCurrentElement]["type"]==ELEMENT_TYPE_LINE:
                # Make bezier
                numX0=arrElements[numCurrentElement-1]["coords"][0]
                numY0=arrElements[numCurrentElement-1]["coords"][1]
                numX1=arrElements[numCurrentElement]["coords"][0]
                numY1=arrElements[numCurrentElement]["coords"][1]
                numDX=numX0-numX1
                numDY=numY0-numY1
                dx3rd=numDX/3.0
                dy3rd=numDY/3.0
                h1x=numX0-(dx3rd*1)
                h1y=numY0-(dy3rd*1)
                h2x=numX0-(dx3rd*2)
                h2y=numY0-(dy3rd*2)
                # Make handle coords relative
                h1x-=numX0
                h1y-=numY0
                h2x-=numX1
                h2y-=numY1
                arrElements[numCurrentElement]["coords"].append(h1x)
                arrElements[numCurrentElement]["coords"].append(h1y)
                arrElements[numCurrentElement]["coords"].append(h2x)
                arrElements[numCurrentElement]["coords"].append(h2y)
                arrElements[numCurrentElement]["type"]=ELEMENT_TYPE_BEZIER
                arrElements[numCurrentElement]["bezier_steps"]=numDefaultBezierSteps
          elif event.key==K_t:
            numCurrentHandle+=1
            if numCurrentHandle>2:
              numCurrentHandle=HANDLE_ID_OTHER_END_CURVE
          elif event.key==K_o:
            # Open file
            boolGotFilename=True
            strFilename=GetOpenFileName()
            #print "Open file \""+str(strFilename)+"\""
            if strFilename=="" or strFilename==None:
              strWaitingForClickMessage="Open file cancelled! Click to continue."
              boolGotFilename=False
            if boolGotFilename:
              boolFileLoadedOk=True
              # Test infile
              numResult=xia.CheckFileExistsAndReadable(strFilename)
              if numResult!=0:
                if numResult&xia.FILE_EXISTS_FAIL:
                  strWaitingForClickMessage="Error: Can't find file \""+strFilename+"\"!\nClick to continue."
                  boolFileLoadedOk=False
                if numResult&xia.FILE_READABLE_FAIL:
                  strWaitingForClickMessage="Error: Can't read file \""+strFilename+"\"!\nClick to continue."
                  boolFileLoadedOk=False
              if boolFileLoadedOk:
                LoadFile(strFilename)
                if arrElements=="--":
                  strWaitingForClickMessage="Something really weird went wrong reading file \""+strFilename+"\"!\nClick to continue."
                else:
                  strWaitingForClickMessage="Opened file \""+strFilename+"\".\nClick to continue."
                  pygame.display.set_caption("Pathologist.py v"+strVersion+" - "+strFilename)
            numMode=EDITOR_MODE_WAIT_FOR_CLICK

#          elif event.key==K_e:
#            # Export to dc.w
#            BuildPlotPreviewPath()
#            if not arrOutPlots:
#              strWaitingForClickMessage="No data to export!\nClick to continue."
#              numMode=EDITOR_MODE_WAIT_FOR_CLICK
#            else:
#              boolOutOfRange=False
#              arrResult=BuildExportData(arrOutPlots, strFilename)
#              if arrResult["success"]==False:
#                boolOutOfRange=False
#              else:
#                arrExportData=arrResult["data"]
#
#              strWaitingForClickMessage="Data exported to \""+strFilename+"\".\nClick to continue."
#
#              if boolOutOfRange:
#                strWaitingForClickMessage="Some data is outside word range (-32768 -- 32767) - export cancelled!\nClick to continue."
#                numMode=EDITOR_MODE_WAIT_FOR_CLICK
#              else:
#                strFilename=GetExportFileName(strLatestExportFilename)
#                boolGotFilename=True
#                if strFilename=="" or strFilename==None:
#                  strWaitingForClickMessage="Exporting cancelled! Click to continue."
#                  boolGotFilename=False
#                else:
#                  strLatestExportFilename=strFilename
#                  if not xia.EndsWith(strFilename, ".s"):
#                    strFilename+=".s"
#                  xia.WriteTextFile(arrExportData, strFilename)
#                  strWaitingForClickMessage="Data exported to \""+strFilename+"\".\nClick to continue."
#                numMode=EDITOR_MODE_WAIT_FOR_CLICK

          elif event.key==K_s:
             # Save
            strFilename=GetSaveFileName(strLatestPathFilename)
            boolGotFilename=True
            if strFilename=="" or strFilename==None:
              strWaitingForClickMessage="Saving cancelled! Click to continue."
              boolGotFilename=False
            else:
              if not xia.EndsWith(strFilename, ".path"):
                strFilename+=".path"
              #print "\nFilename\n--------------------\n"+str(strFilename)
              arrSaveData={}
              arrSaveData["program_version"]="Pathologist v"+strVersion
              arrSaveData["target_screen_width"]=numTargetScreenWidth
              arrSaveData["target_screen_height"]=numTargetScreenHeight
              arrSaveData["elements"]=arrElements
              arrSaveData["framerate"]=numPlaybackFrameRate
              n=numCurrentElement
              if n==None:
                n=0
              arrSaveData["current_element"]=n
              strJSON=json.dumps(arrSaveData, indent=2, separators=(",", ": "))
              arrJSON=strJSON.split("\n")
              xia.WriteTextFile(arrJSON, strFilename)
              strLatestPathFilename=strFilename
              strLatestExportFilename=strFilename
              if xia.EndsWith(strLatestExportFilename, ".path"):
                strLatestExportFilename=strLatestExportFilename[:-len(".path")]
#              print "\n\narrJSON:"
#              for t in arrJSON:
#                print t
              strWaitingForClickMessage="File saved as \""+strFilename+"\".\nClick to continue."
              pygame.display.set_caption("Pathologist.py v"+strVersion+" - "+strFilename)
            numMode=EDITOR_MODE_WAIT_FOR_CLICK
          elif event.key==K_l:
            # Convert current element to line
            if arrElements:
              if arrElements[numCurrentElement]["type"]==ELEMENT_TYPE_BEZIER:
                arrElements[numCurrentElement]["type"]=ELEMENT_TYPE_LINE
                arrElements[numCurrentElement]["coords"]=arrElements[numCurrentElement]["coords"][:2]
          elif event.key==K_PAGEUP:
            if arrElements:
              if arrElements[numCurrentElement]["type"]==ELEMENT_TYPE_BEZIER:
                if numKeyboardModifiers&(KMOD_SHIFT):
                  arrElements[numCurrentElement]["bezier_steps"]+=1
                else:
                  arrElements[numCurrentElement]["bezier_steps"]+=10
          elif event.key==K_PAGEDOWN:
            if arrElements:
              if arrElements[numCurrentElement]["type"]==ELEMENT_TYPE_BEZIER:
                if numKeyboardModifiers&(KMOD_SHIFT):
                  arrElements[numCurrentElement]["bezier_steps"]-=1
                else:
                  arrElements[numCurrentElement]["bezier_steps"]-=10
            if arrElements[numCurrentElement]["bezier_steps"]<numBezierStepsMinimum:
              arrElements[numCurrentElement]["bezier_steps"]=numBezierStepsMinimum
          elif event.key==K_LEFT:
            if arrElements:
              if numCurrentElement>0:
                numCurrentElement-=1
          elif event.key==K_RIGHT:
            if arrElements:
              if numCurrentElement<len(arrElements)-1:
                numCurrentElement+=1
          elif event.key==K_g:
            if numCurrentElement!=None:
              if arrElements:
                thisElement=arrElements[numCurrentElement]
                if thisElement["type"]==ELEMENT_TYPE_BEZIER:
                  prevElement=arrElements[numCurrentElement-1]
                  numX0=prevElement["coords"][0]
                  numY0=prevElement["coords"][1]
                  numX1=thisElement["coords"][0]
                  numY1=thisElement["coords"][1]
                  if numCurrentHandle==HANDLE_ID_OTHER_END_CURVE:
                    # Previous vertex curve handle
                    numX=thisElement["coords"][2]+numX0
                    numY=thisElement["coords"][3]+numY0
                  elif numCurrentHandle==HANDLE_ID_THIS_END_CURVE:
                    # Current vertex curve handle
                    numX=thisElement["coords"][4]+numX1
                    numY=thisElement["coords"][5]+numY1
                  elif numCurrentHandle==HANDLE_ID_VERTEX:
                    numX=thisElement["coords"][0]
                    numY=thisElement["coords"][1]
                elif thisElement["type"]==ELEMENT_TYPE_START or thisElement["type"]==ELEMENT_TYPE_LINE:
                  numX=thisElement["coords"][0]
                  numY=thisElement["coords"][1]
                numX*=numTargetScale
                numY*=numTargetScale
                pygame.mouse.set_pos(numX+twtopx, numY+twtopy)
                numMode=EDITOR_MODE_VERTEX_GRAB


    # Handle handling
    arrHandles=[]
    if arrElements:
      for i in range(len(arrElements)):
        e=arrElements[i]
        if i>0:
          f=arrElements[i-1]
          numX0=f["coords"][0]
          numY0=f["coords"][1]
        else:
          numX0=0
          numY0=0
        numX1=e["coords"][0]
        numY1=e["coords"][1]
        thisHandle={}
        thisHandle["type"]=HANDLE_TYPE_VERTEX
        thisHandle["x"]=numX1
        thisHandle["y"]=numY1
        thisHandle["handle_id"]=HANDLE_ID_VERTEX
        thisHandle["element"]=i
        boolSelected=False
        if numCurrentElement==i and numCurrentHandle==HANDLE_ID_VERTEX:
          boolSelected=True
        thisHandle["selected"]=boolSelected
        arrHandles.append(thisHandle)
        if e["type"]==ELEMENT_TYPE_BEZIER:
          cHandle1={}
          cHandle1["type"]=HANDLE_TYPE_CURVE
          cHandle1["x"]=e["coords"][2]+numX0
          cHandle1["y"]=e["coords"][3]+numY0
          cHandle1["handle_id"]=HANDLE_ID_OTHER_END_CURVE
          cHandle1["element"]=i
          boolSelected=False
          if numCurrentElement==i and numCurrentHandle==HANDLE_ID_OTHER_END_CURVE:
            boolSelected=True
          cHandle1["selected"]=boolSelected
          arrHandles.append(cHandle1)
          cHandle2={}
          cHandle2["type"]=HANDLE_TYPE_CURVE
          cHandle2["x"]=e["coords"][4]+numX1
          cHandle2["y"]=e["coords"][5]+numY1
          cHandle2["handle_id"]=HANDLE_ID_THIS_END_CURVE
          cHandle2["element"]=i
          boolSelected=False
          if numCurrentElement==i and numCurrentHandle==HANDLE_ID_THIS_END_CURVE:
            boolSelected=True
          cHandle2["selected"]=boolSelected
          arrHandles.append(cHandle2)

    #print arrHandles


    ###########################################################################
    ###########################################################################
    ## Render


    ###########################################################################
    ## Draw background


    screen.fill(colBackground)


    ## Draw background
    ###########################################################################


    arrPlots=[]

    boolRenderEditorStuff=True
    boolRenderPlotPreview=False
    boolRenderAnimatedPreview=False
    boolRenderWaitForClickText=False
    boolRenderAreYouSureYouWantToQuitText=False
    boolShowInfoText=True

    if numMode==EDITOR_MODE_PLOT_PREVIEW:
      boolRenderEditorStuff=False
      boolRenderPlotPreview=True

    if numMode==EDITOR_MODE_WAIT_FOR_CLICK:
      boolRenderEditorStuff=False
      boolShowInfoText=False
      boolRenderWaitForClickText=True
    
    if numMode==EDITOR_MODE_ANIMATED_PREVIEW:
      boolRenderEditorStuff=False
      boolRenderAnimatedPreview=True
      
    if numMode==EDITOR_MODE_QUIT_ARE_YOU_SURE:
      boolRenderEditorStuff=False
      boolRenderAreYouSureYouWantToQuitText=True


    ###########################################################################
    ## Draw target window


    if boolRenderEditorStuff or boolRenderPlotPreview or boolRenderAnimatedPreview:
      # Border of target rectangle
      screen.fill(colTargetFrame, (twtopx-bs, twtopy-bs, tw+(bs*2), th+(bs*2)))
      # Inside of target rectangle
      screen.fill(colTargetWindow, (twtopx, twtopy, tw, th))


    ## Draw target window
    ###########################################################################
    ## Draw target window contents


    ###########################################################
    ## Bezier handle lines


    if boolRenderEditorStuff:
      if arrElements:
        for i in range(len(arrElements)):
          if i==numCurrentElement or 1==1:
            thisElement=arrElements[i]
            if thisElement["type"]==ELEMENT_TYPE_BEZIER:
              prevElement=arrElements[i-1]
              c=thisElement["coords"]
              numX0=prevElement["coords"][0]
              numY0=prevElement["coords"][1]
              numX1=thisElement["coords"][0]
              numY1=thisElement["coords"][1]
              destx0=c[2]+numX0
              desty0=c[3]+numY0
              destx1=c[4]+numX1
              desty1=c[5]+numY1
              numX0*=numTargetScale
              numY0*=numTargetScale
              numX1*=numTargetScale
              numY1*=numTargetScale
              destx0*=numTargetScale
              desty0*=numTargetScale
              destx1*=numTargetScale
              desty1*=numTargetScale
              numX0+=twtopx+(numTargetScale/2.0)
              numY0+=twtopy+(numTargetScale/2.0)
              numX1+=twtopx+(numTargetScale/2.0)
              numY1+=twtopy+(numTargetScale/2.0)
              destx0+=twtopx+(numTargetScale/2.0)
              desty0+=twtopy+(numTargetScale/2.0)
              destx1+=twtopx+(numTargetScale/2.0)
              desty1+=twtopy+(numTargetScale/2.0)
              pygame.draw.aaline(screen, colBezierHelpline, (numX0, numY0), (destx0, desty0))
              pygame.draw.aaline(screen, colBezierHelpline, (numX1, numY1), (destx1, desty1))


    ## Bezier handle lines
    ###########################################################
    ## Lines and beziers


    if boolRenderEditorStuff:
      RenderLinesAndBeziers()


    ## Lines and beziers
    ###########################################################
    ## Selected line/bezier


    if boolRenderEditorStuff:
      if numCurrentElement!=None:
        if arrElements:
          for i in range(len(arrElements)):
            if numCurrentElement==i:
              thisElement=arrElements[i]
              if thisElement["type"]==ELEMENT_TYPE_START:
                if len(arrElements)>i+1:
                  nextElement=arrElements[i+1]
                  if nextElement["type"]==ELEMENT_TYPE_LINE:
                    arrCoords=thisElement["coords"]
                    arrNextCoords=nextElement["coords"]
                    numSpeed=thisElement["speed"]
                    DrawBresenham(arrCoords[0], arrCoords[1], arrNextCoords[0], arrNextCoords[1], colLineSelected, numSpeed, True, i)
                  elif thisElement["type"]==ELEMENT_TYPE_BEZIER:
                    arrCoords=thisElement["coords"]
                    arrNextCoords=nextElement["coords"]
                    #Draw Bezier()
              elif thisElement["type"]==ELEMENT_TYPE_LINE:
                prevElement=arrElements[i-1]
                arrPrevCoords=prevElement["coords"]
                arrCoords=thisElement["coords"]
                numSpeed=thisElement["speed"]
                DrawBresenham(arrPrevCoords[0], arrPrevCoords[1], arrCoords[0], arrCoords[1], colLineSelected, numSpeed, True, i)
              elif thisElement["type"]==ELEMENT_TYPE_BEZIER:
                prevElement=arrElements[i-1]
                numX0=prevElement["coords"][0]
                numY0=prevElement["coords"][1]
                numX1=thisElement["coords"][0]
                numY1=thisElement["coords"][1]
                p=prevElement["coords"]
                c=thisElement["coords"]
                numSpeed=thisElement["speed"]
                DrawBezier(p[0], p[1], c[0], c[1], c[2]+numX0, c[3]+numY0, c[4]+numX1, c[5]+numY1, colLineSelected, numSpeed, True, i)


    ## Selected line/bezier
    ###########################################################
    ## Handles


    if boolRenderEditorStuff:
      if arrHandles:
        for h in arrHandles:
          numType=h["type"]
          numX=h["x"]
          numY=h["y"]
          boolSelected=h["selected"]
          if h["element"]!=numCurrentElement:
            boolSelected=False
          if h["element"]==0 and numCurrentElement==0:
            boolSelected=True
          DrawHandle(numX, numY, numType, boolSelected)


    ## Handles
    ###########################################################
    ## Plot preview


    if boolRenderPlotPreview:
      for p in arrOutPlots:
        PutTargetPixel(p["xy"][0], p["xy"][1], colPreviewPlot)


    ## Plot preview
    ###########################################################
    ## Wait-For-Click text


    if boolRenderWaitForClickText:
      DrawText(strWaitingForClickMessage, numInfoPositionX, numInfoPositionY, colText)


    ## Wait-For-Click text
    ###########################################################
    ## Animated preview


    if boolRenderAnimatedPreview:
      if numAnimFrame>=len(arrOutPlots):
        numAnimFrame=0
      pos=arrOutPlots[numAnimFrame]["xy"]
      x=pos[0]
      y=pos[1]
      x*=numTargetScale
      y*=numTargetScale
      x+=twtopx+(numTargetScale/2.0)
      y+=twtopy+(numTargetScale/2.0)
      x=int(x)
      y=int(y)
      numRadius=30
      numWidth=4
      colThis=(255, 255, 255)
      pygame.draw.circle(screen, colThis, (x, y), numRadius, numWidth)
      
      numAnimFrame+=1


    ## Animated preview
    ###########################################################
    ## Test stuff


    ## Test stuff
    ###########################################################

    ## Draw target window contents
    ###########################################################################
    ## Draw info texts


    if boolShowInfoText:
      boolShowDistanceToPrevious=False  # True
      numTotalPoints=0
      for i, thisElement in enumerate(arrElements):
        if i!=0 or 1==1:
          numTotalPoints+=thisElement["bezier_steps"]
      strMouseInfo="Mouse: ("+str(numTargetMouseX).zfill(4)+", "+str(numTargetMouseY).zfill(4)+")"
      strScaleInfo="Scale (mousewheel): ("+arrTargetScales[numTargetScaleIndex]["str"]+")"
      strTotalPoints="Final points: "+str(numTotalPoints)
      #if arrElements and numCurrentElement:  # Allow changing of element 0
      if arrElements:
        sp=arrElements[numCurrentElement]["speed"]
        sp=round(sp, 3)
      else:
        sp=0
      strCurrentElement="Current element: "+str(numCurrentElement)+"/"+str(len(arrElements)-1)
      if len(arrElements)>0:
        thisElement=arrElements[numCurrentElement]
        numX=thisElement["coords"][0]
        numY=thisElement["coords"][1]
        strVxy="Vertex: ("+str(numX)+", "+str(numY)+")"
        numType=thisElement["type"]
        if numType==ELEMENT_TYPE_BEZIER:
          numH0X=thisElement["coords"][2]
          numH0Y=thisElement["coords"][3]
          numH1X=thisElement["coords"][4]
          numH1Y=thisElement["coords"][5]
          strH0="("+str(int(numH0X))+", "+str(int(numH0Y))+")"
          strH1="("+str(int(numH1X))+", "+str(int(numH1Y))+")"
          strCurrentElement+=" (bezier) "+strVxy+", handle: "+strH0+", other handle: "+strH1
        elif numType==ELEMENT_TYPE_LINE:
          strCurrentElement+=" (line) "+strVxy
        elif numType==ELEMENT_TYPE_START:
          strCurrentElement+=" (start) "+strVxy
        if numType==ELEMENT_TYPE_BEZIER:
          c=arrElements[numCurrentElement]["bezier_steps"]
          strCurrentElement+=" (curve steps: "+str(c)+")"
        strCurrentElement+=" (speed: "+str(sp)+")"
        if boolShowDistanceToPrevious:
          # Distance to previous element
          if numCurrentElement>0:
            prevElement=arrElements[numCurrentElement-1]
            d=GetDistance(thisElement["coords"], prevElement["coords"])
            strCurrentElement+=" dist to prev: "+str(d)
      
      
      strKeys="\nLeft/Right=select current element, G=grab vertex"
      #strKeys+=", L=add line element at end"
      strKeys+=", Del=delete current"
      strKeys+=", I=insert element"
      strKeys+=", L=make line"
      strKeys+=", C=make curve"
      strKeys+=", O/S=Open/Save file"
      strKeys+=", CTRL+click to add line at end"


      strDebug=""
      #strDebug="arrhandles: "+str(len(arrHandles))+" "
      #strDebug="current handle: "+str(numCurrentHandle)+" "
      strText=strDebug
      strText+=strMouseInfo+" | "+strScaleInfo+ " | "+strTotalPoints
      if len(arrElements)>0:
        strText+=" | "+strCurrentElement
      if numMode==EDITOR_MODE_NORMAL:
        strText+=strKeys
      if numMode==EDITOR_MODE_PLACE_LINE_START:
        strText+="\nSet line start with click"
      elif numMode==EDITOR_MODE_PLACE_LINE_END:
        strText+="\nSet line end with click"
      elif numMode==EDITOR_MODE_VERTEX_GRAB:
        strText+="\nMove vertex, click to confirm"
      elif numMode==EDITOR_MODE_SET_SPEED:
        #strText+="\nEdit GLOBAL speed with Up/Down arrow, shift for 0.1 steps, ctrl for 0.01 steps, Enter to set, Esc to cancel (speed="+str(numEditedSpeed)+")"
        strText="Edit GLOBAL speed with Up/Down arrow, shift for 0.1 steps, ctrl for 0.01 steps, R to round value, Enter to set, Esc to cancel (speed="+str(numEditedSpeed)+")"
      elif numMode==EDITOR_MODE_PLOT_PREVIEW:
        strText+="\nPlot preview mode ("+str(len(arrOutPlots))+" points). Press P or Esc to leave."
      elif boolRenderAreYouSureYouWantToQuitText:
        strText="Are you sure you want to quit? (Y/N)"
      elif boolRenderAnimatedPreview:
        numMs=float(myClock.get_time())
        numActualFrameRate=(1.0/numMs)*1000.0
        numActualFrameRate=round(numActualFrameRate, 1)
        strText="Framerate, expected: "+str(numFrameRate)+", actual: "+str(numActualFrameRate)
        strText+="\nAnimated preview mode. Press Up/Down keys to change framerate, A or Esc to leave."
      
      DrawText(strText, numInfoPositionX, numInfoPositionY, colText)


    ## Draw info texts
    ###########################################################################
    ## Finish up and flip


    myClock.tick(numFrameRate)
    pygame.display.flip()


    ## Finish up and flip
    ###########################################################################


    ## Render
    ###########################################################################
    ###########################################################################


## Main loop
################################################################################################
################################################################################################

# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
  # call the main function
  main()

